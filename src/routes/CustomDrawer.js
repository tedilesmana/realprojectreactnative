import React, {useState} from 'react';
import {View, StyleSheet, Image, Text, Switch} from 'react-native';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const CustomDrawer = (props, {navigation}) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  return (
    <DrawerContentScrollView {...props}>
      <View style={styles.drawerContent}>
        <View style={styles.userInfoSection}>
          <View style={styles.userInfo}>
            <Image
              source={{
                uri:
                  'https://i.pinimg.com/originals/ff/a0/9a/ffa09aec412db3f54deadf1b3781de2a.png',
              }}
              style={styles.avatar}
            />
            <View style={styles.name}>
              <Text style={styles.title}>John Doe</Text>
              <Text style={styles.caption}>@j_doe</Text>
            </View>
          </View>

          <View style={styles.row}>
            <View style={styles.section}>
              <Text style={[styles.paragraph, styles.caption]}>80</Text>
              <Text style={styles.caption}>Following</Text>
            </View>
            <View style={styles.section}>
              <Text style={[styles.paragraph, styles.caption]}>100</Text>
              <Text style={styles.caption}>Followers</Text>
            </View>
          </View>
        </View>

        <View style={styles.drawerSection}>
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="home-outline" color={color} size={size} />
            )}
            label="Home"
            onPress={() => {
              props.navigation.navigate('Home');
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="account-outline" color={color} size={size} />
            )}
            label="Profile"
            onPress={() => {
              props.navigation.navigate('Profile');
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="bookmark-outline" color={color} size={size} />
            )}
            label="Bookmarks"
            onPress={() => {
              props.navigation.navigate('BookmarkScreen');
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="account-child" color={color} size={size} />
            )}
            label="Settings"
            onPress={() => {
              props.navigation.navigate('SettingsScreen');
            }}
          />
          <DrawerItem
            icon={({color, size}) => (
              <Icon name="account-check-outline" color={color} size={size} />
            )}
            label="Support"
            onPress={() => {
              props.navigation.navigate('SupportScreen');
            }}
          />
        </View>
        <View title="Preferences">
          <View style={styles.container}>
            <Switch
              trackColor={{false: '#767577', true: '#81b0ff'}}
              thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>
      </View>
      <View style={styles.bottomDrawerSection}>
        <DrawerItem
          icon={({color, size}) => (
            <Icon name="exit-to-app" color={color} size={size} />
          )}
          label="Sign Out"
          onPress={() => {}}
        />
      </View>
    </DrawerContentScrollView>
  );
};

export default CustomDrawer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    marginVertical: 30,
  },
  avatar: {
    height: 50,
    width: 50,
  },
  userInfo: {
    marginTop: 15,
    flexDirection: 'row',
  },
  name: {
    marginLeft: 15,
    flexDirection: 'column',
  },
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    marginHorizontal: 20,
    borderBottomWidth: 1,
    paddingBottom: 20,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
    color: 'black',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    color: 'black',
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
