import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';

import StackNavigation from './StackNavigation';
import CustomDrawer from './CustomDrawer';

const Drawer = createDrawerNavigator();

export default function MainNavigation() {
  return (
    <Drawer.Navigator
      drawerType="front"
      initialRouteName="Main"
      drawerContent={(props) => <CustomDrawer {...props} />}>
      <Drawer.Screen name="Main" component={StackNavigation} />
    </Drawer.Navigator>
  );
}
