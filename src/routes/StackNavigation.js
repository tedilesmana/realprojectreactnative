import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {StyleSheet} from 'react-native';

import OnboardingScreen from '../screens/OnboardingScreen';
import LoginScreen from '../screens/LoginScreen';
import BottomNavigation from './BottomNavigation';

const Stack = createStackNavigator();

const StackNavigation = ({navigation}) => (
  <Stack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#009387',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <Stack.Screen
      name="Onboarding"
      component={OnboardingScreen}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Login"
      component={LoginScreen}
      options={{
        headerShown: false,
      }}
    />
    <Stack.Screen
      name="Home"
      component={BottomNavigation}
      options={{
        title: 'OceanStore',
        headerTitleStyle: {
          fontWeight: 'normal',
        },
        headerLeft: () => (
          <Ionicons
            name="ios-apps-outline"
            size={25}
            color="#fff"
            onPress={() => navigation.openDrawer()}
            style={styles.drawerMenu}
          />
        ),
      }}
    />
  </Stack.Navigator>
);

export default StackNavigation;

const styles = StyleSheet.create({
  drawerMenu: {
    marginLeft: 20,
  },
});
