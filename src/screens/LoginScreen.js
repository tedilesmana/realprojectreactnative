import React, {useState, useEffect, useReducer, useCallback} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Alert,
  Text,
} from 'react-native';
import {useDispatch} from 'react-redux';

import Input from '../components/general/Input';
import * as authActions from '../stores/actions/auth';
import customStyle from '../constants/CustomStyles';
import PillButton from '../components/general/PillButton';
import {TouchableOpacity} from 'react-native-gesture-handler';
import formHandling from '../stores/reducers/formHandling';

export default function LoginScreen(props) {
  const dispatch = useDispatch();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isSignup, setIsSignup] = useState(false);

  const [formState, dispatchFormState] = useReducer(formHandling, {
    inputValues: null,
    inputValidities: null,
    formIsValid: false,
  });

  const inputChangeHandler = useCallback(
    (inputIdentifier, inputValue, inputValidity) => {
      dispatchFormState({
        type: 'FORM_INPUT_UPDATE',
        value: inputValue,
        isValid: inputValidity,
        input: inputIdentifier,
      });
    },
    [dispatchFormState],
  );

  const authHandler = async () => {
    let action;
    if (isSignup) {
      action = authActions.signup(
        formState.inputValues.email,
        formState.inputValues.password,
      );
    } else {
      action = authActions.login(
        formState.inputValues.email,
        formState.inputValues.password,
      );
    }
    setError(null);
    setIsLoading(true);
    try {
      await dispatch(action);
      props.navigation.navigate('Home');
    } catch (err) {
      setError(err.message);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (error) {
      Alert.alert('An Error Occurred!', error, [{text: 'Okay'}]);
    }
  }, [error]);

  return (
    <View style={customStyle.middle}>
      <View style={[styles.formContainer, customStyle.card]}>
        {isSignup ? (
          <Text style={styles.title}>LOGIN</Text>
        ) : (
          <Text style={styles.title}>LOGIN</Text>
        )}
        <ScrollView>
          <Input
            id="email"
            label="E-mail"
            keyboardType="email-address"
            required
            email
            autoCapitalize="none"
            errorText="Please enter a valid email address."
            onInputChange={inputChangeHandler}
            initialValue=""
          />
          <Input
            id="password"
            label="Password"
            keyboardType="default"
            secureTextEntry
            required
            minLength={5}
            autoCapitalize="none"
            errorText="Please enter a valid password."
            onInputChange={inputChangeHandler}
            initialValue=""
          />
        </ScrollView>
        <View style={styles.buttonContainer}>
          {isLoading ? (
            <ActivityIndicator size="small" color="red" />
          ) : (
            <PillButton
              title={isSignup ? 'SignUp' : 'Login'}
              // onPress={() => authHandler()}
              onPress={() => {
                props.navigation.navigate('Home');
              }}
            />
          )}
        </View>
        {isSignup ? (
          <View style={styles.textButtonContainer}>
            <Text>Anda Sudah Memiliki Akun ?</Text>
            <TouchableOpacity
              onPress={() => {
                setIsSignup((prevState) => !prevState);
              }}>
              <Text style={styles.textLink}>Login</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={styles.textButtonContainer}>
            <Text>Anda Belum Memiliki Akun ?</Text>
            <TouchableOpacity
              onPress={() => {
                setIsSignup((prevState) => !prevState);
              }}>
              <Text style={styles.textLink}>Sign Up</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 30,
    marginTop: 20,
  },
  formContainer: {
    width: '80%',
    padding: 20,
  },
  buttonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  textButtonContainer: {
    marginVertical: 30,
    alignItems: 'center',
  },
  textLink: {
    color: 'blue',
  },
});
