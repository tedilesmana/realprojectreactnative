import React, {useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import Onboarding from 'react-native-onboarding-swiper';
import LoginScreen from './LoginScreen';

const Dots = ({selected}) => {
  let backgroundColor;

  backgroundColor = selected ? 'rgba(0, 0, 0, 0.8)' : 'rgba(0, 0, 0, 0.3)';

  return <View style={[styles.dots, {backgroundColor}]} />;
};

const Skip = ({...props}) => {
  return (
    <TouchableOpacity {...props}>
      <Text style={styles.button}>Skip</Text>
    </TouchableOpacity>
  );
};

const Next = ({...props}) => {
  return (
    <TouchableOpacity {...props}>
      <Text style={styles.button}>Next</Text>
    </TouchableOpacity>
  );
};

const Done = ({...props}) => {
  return (
    <TouchableOpacity {...props}>
      <Text style={styles.button}>Done</Text>
    </TouchableOpacity>
  );
};

export default function OnboardingScreen({navigation}) {
  const [isFirstLaunch, setIsFirstLaunch] = React.useState(null);
  useEffect(() => {
    AsyncStorage.getItem('alreadyLaunched').then((value) => {
      if (value == null) {
        AsyncStorage.setItem('alreadyLaunched', 'true');
        setIsFirstLaunch(true);
      } else {
        setIsFirstLaunch(false);
      }
    });
  }, []);

  if (isFirstLaunch === null) {
    return null;
  } else if (isFirstLaunch === true) {
    return (
      <Onboarding
        SkipButtonComponent={Skip}
        NextButtonComponent={Next}
        DoneButtonComponent={Done}
        DotComponent={Dots}
        onSkip={() => {
          navigation.replace('Login');
        }}
        onDone={() => {
          navigation.replace('Login');
        }}
        pages={[
          {
            backgroundColor: '#7ff964',
            image: (
              <Image
                source={require('../assets/images/onboarding/onboarding-img1.png')}
              />
            ),
            title: 'Welcome',
            subtitle: 'To Our Application',
          },
          {
            backgroundColor: '#fff445',
            image: (
              <Image
                source={require('../assets/images/onboarding/onboarding-img2.png')}
              />
            ),
            title: 'Our Connect You',
            subtitle: 'With Another People in Around The World',
          },
          {
            backgroundColor: '#f456ff',
            image: (
              <Image
                source={require('../assets/images/onboarding/onboarding-img3.png')}
              />
            ),
            title: 'Join Us',
            subtitle: 'To Get Our All Offering for You',
          },
        ]}
      />
    );
  } else {
    return <LoginScreen navigation={navigation} />;
  }
}

const styles = StyleSheet.create({
  button: {
    marginHorizontal: 15,
  },
  dots: {
    width: 6,
    height: 6,
    borderRadius: 3,
    marginHorizontal: 3,
  },
});
