import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

export default function PillButton(props) {
  return (
    <TouchableOpacity onPress={props.onPress}>
      <View style={styles.button} onPress={props.onPress}>
        <Text style={styles.textButton}>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 40,
    paddingVertical: 10,
    backgroundColor: 'green',
    borderRadius: 50,
  },
  textButton: {
    color: 'white',
  },
});
